package fr.uavignon.ceri.tp3.data.webservice;

import fr.uavignon.ceri.tp3.data.WeatherResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Path;

public interface OWMInterface {
    @Headers("Accept: application/geo+json")
    @GET("/gridpoints/{city}/{temp},{humidite}/forecast")
    Call<WeatherResponse> getForecast(@Path("city") String city,
                                      @Path("temp") float temp,
                                      @Path("humidite") int humidite);
}
