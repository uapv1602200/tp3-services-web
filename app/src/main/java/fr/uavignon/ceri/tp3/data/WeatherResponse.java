package fr.uavignon.ceri.tp3.data;

import java.util.List;

import fr.uavignon.ceri.tp3.data.Forecast;

public class WeatherResponse {
    public final Properties properties=null;

    public static class Properties {
        public final List<Forecast> periods=null;

    }
}
