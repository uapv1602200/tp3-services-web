package fr.uavignon.ceri.tp3.data;

import java.util.List;

import fr.uavignon.ceri.tp3.data.City;
import fr.uavignon.ceri.tp3.data.Forecast;
import fr.uavignon.ceri.tp3.data.WeatherResponse;

public class WeatherResult {
    public final boolean isLoading;
    public final List<Forecast> forecasts;
    public final Throwable error;


    WeatherResult(boolean isLoading, List<Forecast> forecasts, Throwable error) {
        this.isLoading = isLoading;
        this.forecasts = forecasts;
        this.error = error;
    }

    public static void transferInfo(WeatherResponse weatherInfo, City cityInfo) {

    }
}
